﻿using UnityEngine;
using System.Collections;

public class BrickResistance : MonoBehaviour {

    [SerializeField]
    private int _maxHits;
    [SerializeField]
    private Sprite[] hitSprites;

    private LevelManager lvlManager;

    private int _hits;
    private SpriteRenderer spRenderer;

    void Start() {
        lvlManager = FindObjectOfType<LevelManager>();
        lvlManager.addBrick();

        spRenderer = GetComponent<SpriteRenderer>();
    }

    void OnCollisionEnter2D(Collision2D col) {
        _hits++;
        if(_hits >= _maxHits) {
            lvlManager.substractBrick();
            Destroy(gameObject);
        } else {
            spRenderer.sprite = hitSprites[_hits - 1];
        }
    }
}
