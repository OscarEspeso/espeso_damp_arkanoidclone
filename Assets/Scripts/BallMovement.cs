﻿using UnityEngine;
using System.Collections;

public class BallMovement : MonoBehaviour {

    [SerializeField]
    private bool ballFree;
    private Rigidbody2D rb;
    private Transform paddleTr;

    private float initPosY;

    [SerializeField]
    private float _velocity;
    private Vector2 vel;

    // Use this for initialization
	void Start () {
        initPosY = transform.position.y;
        ballFree = false;
        // Convertir el rigidbody en cinemático
        rb = GetComponent<Rigidbody2D>();
        vel = new Vector2(0, 6);

        PaddleMovement paddle = FindObjectOfType<PaddleMovement>();
        paddleTr = paddle.transform;
	}
	
	// Update is called once per frame
	void Update () {
	    // Si ballFree false
        if (!ballFree) {
            rb.isKinematic = true;
            // posicion x igual a posicion x de paleta
            transform.position = new Vector3(paddleTr.position.x, initPosY, transform.position.z);
            if (Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Space)) {
                ballFree = true;
                rb.isKinematic = false;
                rb.velocity = vel;
            }
        }
           
        // 
	}

    void OnTriggerEnter2D(Collider2D col) {
        if (col.name == "DeathTrigger") {
            ballFree = false;
        }
    }
}
