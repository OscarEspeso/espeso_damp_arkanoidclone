﻿using UnityEngine;
using System.Collections;

public class LooseTrigger : MonoBehaviour {

    private PlayerInfo playerInfo;

	void Start () {
        playerInfo = FindObjectOfType<PlayerInfo>();
	}

	void OnTriggerEnter2D(Collider2D col) {
        playerInfo.lives--;
        if(playerInfo.lives == 0) {
            print("Estás muertooooooooooo!");
            Application.LoadLevel("Start Menu");
        } 
       
    }
}
