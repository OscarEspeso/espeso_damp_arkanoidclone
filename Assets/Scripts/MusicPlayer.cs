﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(AudioSource))]
public class MusicPlayer : Persistant {

    [SerializeField]
    private AudioClip[] songList;
    private AudioSource player;
    private Dictionary<string, AudioClip> songMap;
    private float curClipDuration;
    private float curTime;
    private int curSongInd;

    void Start() {
        
        // Associate file name with uniqyue id for the dictionary
        if (songList != null && songList.Length > 0) {
            songMap = new Dictionary<string, AudioClip>();
            foreach (AudioClip song in songList) {
                songMap.Add(song.name, song);
                
            }
        }

        // Get the audo source as the player
        player = GetComponent<AudioSource>();
        // Start playing firstone
        player.clip = songList[0];
        // Configure so it plays songs inthe array order unless changeSong is called
        player.Play();
        curClipDuration = player.clip.length;
    }

    public void changeSong(string id) {
        AudioClip songClip;
        if (songMap.TryGetValue(id, out songClip)) {
            player.clip = songClip;
        }
    }

    override protected void Update() {
        base.Update();
        if (curTime > curClipDuration) {
            player.clip = songList[(curSongInd < songList.Length - 1) ? ++curSongInd : 0];
            player.Play();
            curTime = 0;
        }
        else {
            curTime += Time.deltaTime;
        }
    }
}
